## Installation

Clone project + submodule(s):

        git clone --recursive https://gitlab.com/craftyguy/esp-meat.git

There's a mpfshell script that can be used to copy the app to a device
connected to a computer:

        mpfshell -o ttyUSB0 -s install.mpfshell

Reset device after copying the files.

## Usage

After the app is installed, it will start up and enable an unsecured wifi AP
called `esp-meat`. Connect a device to this with a web browser, and browse to
`192.168.4.1` to configure the application.
