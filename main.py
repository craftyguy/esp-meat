# Copyright(c) 2020 by craftyguy "Clayton Craft" <clayton@craftyguy.net>
# Distributed under GPLv3+ (see LICENSE) WITHOUT ANY WARRANTY.
import json
import machine
import net_config
import network
import os
import sys
import utime
from lib import LED
from machine import Pin, SPI

# from https://github.com/pfalcon/pycopy-lib/tree/master/umqtt.simple
from umqtt.simple import MQTTClient

# pin for status LED
STATUS_LED_PIN = 14
# pin for powering the power on LED
PWR_LED_PIN = 13
# pin used to toggle transistor powering temp probe sensor (high == transistor on)
TEMP_PROBE_POWER_PIN = 0
# pin for button clearing config (should be pulled low)
RESET_CONFIG_PIN = 12

CONFIG_FILE = "settings.json"
DEFAULT_ESSID = "esp-meat"
SLEEP_INTERVAL_SEC = 10

status_led = LED(STATUS_LED_PIN)
#reset_config = machine.Pin(RESET_CONFIG_PIN, machine.Pin.IN).value()
pwr_led = LED(PWR_LED_PIN)
pwr_led.on()


def make_client(essid=DEFAULT_ESSID, passphrase='',
                hostname=DEFAULT_ESSID):
    s_if = network.WLAN(network.STA_IF)
    a_if = network.WLAN(network.AP_IF)
    if a_if.active():
        a_if.active(False)
    if not s_if.isconnected():
        s_if.active(True)
        s_if.config(dhcp_hostname="esp-motiondetect")
        # Connect to Wifi.
        s_if.connect(essid, passphrase)
        while not s_if.isconnected():
            pass
        print("Wifi connected: ", s_if.ifconfig())


def make_station(essid=DEFAULT_ESSID, passphrase=''):
    s_if = network.WLAN(network.STA_IF)
    s_if.active(False)
    a_if = network.WLAN(network.AP_IF)
    a_if.active(False)
    a_if.active(True)
    old_essid = a_if.config('essid')
    # Channel chosen by fair roll of the dice
    a_if.config(essid=essid, channel=4)
    if passphrase:
        # Use WPA2/PSK
        a_if.config(authmode=3, password=passphrase)
    else:
        # Open network (e.g. for debug or net config)
        a_if.config(authmode=0)
    # Full reset required for a new essid to be applied
    if essid != old_essid:
        machine.reset()


def network_config_wizard(config_file):
    make_station(DEFAULT_ESSID)
    global status_led
    status_led.on()
    net_config.start(config_file)
    machine.reset()

# TODO:
#if reset_config:
#    status_led.pulse(100, 5)
#    # Create a new (or clear an existing) config file
#    # This is so that a reset before new settings are
#    # provided takes us back to the net config wizard
#    with open(CONFIG_FILE, 'w') as f:
#        f.write('')
#    network_config_wizard(CONFIG_FILE)


class Meat():
    def __init__(self):
        self._spi = SPI(1, baudrate=1000000, polarity=0, phase=0)
        self._temp_pwr = Pin(TEMP_PROBE_POWER_PIN, Pin.OUT)

    def get_temp(self):
        self._temp_pwr.on()
        utime.sleep_ms(50)
        val = self._spi.read(2)
        self._temp_pwr.off()
        val_int = int.from_bytes(val, 'big')
        return (val_int >> 2) / 8.0


def connect_network():
    try:
        with open(CONFIG_FILE, 'r') as f:
            config = json.loads(f.read())
    except (OSError, ValueError) as e:
        print("ERR: Unable to read conf file: ")
        print(e)
        network_config_wizard(CONFIG_FILE)

    try:
        net_mode = config['net_mode']
        net_essid = config['net_essid']
        net_pass = config['net_pass']
        if net_mode == 'client':
            net_hostname = config['net_hostname']
    except ValueError as e:
        print("ERR: Unable to read conf file: ")
        print(e)
        network_config_wizard(CONFIG_FILE)

    status_led.pulse(200, 2)
    status_led.on()
    if net_mode == 'ap':
        make_station(net_essid, net_pass)
    else:
        make_client(net_essid, net_pass, net_hostname)
    # Delay 2 seconds for things to settle
    utime.sleep(2)
    status_led.off()


def main(config_file):
    with open(config_file, 'r') as f:
        config = json.load(f)
    # get mqtt server/topic from config
    mqtt_server = config.get('mqtt_server')
    mqtt_topic = config.get('mqtt_topic') or '/esp-meat/temperature'
    try:
        temp_interval = int(config.get('temp_interval'))
    except ValueError:
        temp_interval = SLEEP_INTERVAL_SEC
    # allocate ~4 seconds for network reconnect, etc
    temp_interval -= 4

    if not mqtt_server:
        os.path.remove(config_file)
        raise Exception("ERROR: no mqtt server specified")
    mqtt_server = mqtt_server.split('/')[0]
    mqtt_client = MQTTClient("esp-meat", mqtt_server)

    temp = Meat().get_temp()
    print('temp: ' + str(temp))

    # send temp over mqtt
    attempts = 5
    while attempts > 0:
        try:
            mqtt_client.connect()
            break
        except OSError:
            attempts -= 1
        utime.sleep(1)
    mqtt_client.publish(mqtt_topic.encode(), str(temp).encode())
    mqtt_client.disconnect()
    # delay needed to make sure mqtt message makes it out:
    utime.sleep(.1)

    # deep sleep
    rtc = machine.RTC()
    rtc.irq(trigger=rtc.ALARM0, wake=machine.DEEPSLEEP)
    rtc.alarm(rtc.ALARM0, temp_interval * 1000)
    machine.deepsleep()


if __name__ == "__main__":
    connect_network()
    try:
        main(CONFIG_FILE)
    except Exception as e:
        sys.print_exception(e)
