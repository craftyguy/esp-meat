# Copyright(c) 2018-2020 by craftyguy "Clayton Craft" <clayton@craftyguy.net>
# Distributed under GPLv3+ (see LICENSE) WITHOUT ANY WARRANTY.
import json
# https://github.com/BetaRavener/upy-websocket-server
from ws_connection import ClientClosedError
from ws_server import WebSocketClient, WebSocketServer


class NetConfigClient(WebSocketClient):
    def __init__(self, conn):
        super().__init__(conn)
        self.config_data = None

    def process(self):
        # Process any new messages
        try:
            msg = self.connection.read()
        except ClientClosedError:
            return
        if not msg:
            return
        try:
            json.loads(msg)
        except ValueError:
            self.connection.write('failure parsing json from msg: %s'
                                  % msg)
            # msg was not valid json..
            return
        self.config_data = msg

    def get_config(self):
        return self.config_data


class NetConfigServer(WebSocketServer):
    def __init__(self):
        super().__init__("net_config.html", 8)
        self._client = None

    def _make_client(self, conn):
        self._client = NetConfigClient(conn)
        return self._client

    def get_config(self):
        if self._client:
            return self._client.config_data
        return None


def start(config_file):
    """ Start network config client
    config_file - is a file to write config to (will be overwritten) """
    config_server = NetConfigServer()
    config_server.start()
    try:
        config_data = config_server.get_config()
        while not config_data:
            config_server.process_all()
            config_data = config_server.get_config()
    except KeyboardInterrupt:
        pass
    config_server.stop()
    with open(config_file, 'w') as f:
        print(config_data)
        f.write(config_data)
